#!/usr/bin/python3

import os
import socket
from pathlib import Path

import daemonutil
from logmgr import Log

def fix_str(txt):
    return txt.replace('\t', ' ').replace('\n', ' <\\n> ')

def main():
    options = daemonutil.init_options()
    options.add_argument("-P", "--path", action="store", metavar="PATH", type=Path, help="listening socket path")
    options.add_argument("-D", "--dir", action="store", metavar="PATH", type=Path, help="directory to log into")
    args = options.parse_args()

    args.dir.mkdir(parents=True, exist_ok=True)

    socket_path = args.path

    try:
        with socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM) as sock:
            sock.sendto(b'+\x01!!exit', str(socket_path))
    except OSError:
        pass

    try:
        socket_path.unlink()
    except OSError:
        pass

    sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    sock.bind(str(socket_path))

    os.chdir(args.dir)

    log = Log('cmd-@@.txt', links=True)

    daemonutil.init_from_args(args)

    home = str(Path.home())

    try:
        while True:
            data, addr = sock.recvfrom(8192)
            lst = data.decode('utf8', 'replace').split('\x01', 4)
            if len(lst) == 1:
                lst = ['+'] + data.split(';', 3)

            if lst[1].startswith('!!'):
                cmd = lst[1][2:]
                if cmd == 'exit':
                    return
                continue

            if len(lst) < 4:
                continue

            tag, tty, pid, pwd, cmd = lst
            cmd = cmd.rstrip('\n')

            if cmd.startswith(' ') or not cmd:
                continue

            lst = cmd.split()
            if len(lst) >= 2 and cmd[0].endswith('whatson') and 'enc' in cmd[1]:
                continue

            if tty.startswith('/dev/'):
                tty = tty[5:]
            if pwd.startswith(home):
                pwd = '~' + pwd[len(home):]

            log.log([f'\t{tag}\t{pid:5}\t{fix_str(tty)}\t{fix_str(pwd)}\t{fix_str(cmd)}'])
    except KeyboardInterrupt:
        pass
    finally:
        daemonutil.cleanup()

if __name__ == '__main__':
    main()
