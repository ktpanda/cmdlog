#!/usr/bin/python3
import sys
import time
import os
import argparse
import stat
from pathlib import Path

class Log:
    def __init__(self, pat='', stdout=False, timestdout=False, links=False):
        self.fil = None
        pat = str(pat)
        self.pat = pat
        self.cur_log_path = None
        self.stdout = stdout
        self.timestdout = timestdout
        self.bydate = pat and '@@' in pat
        if links is True:
            links = 'today', 'yesterday'

        self.links = links
        if pat and not self.bydate:
            self.fil = Path(pat).open('a', encoding='utf8')

    def _get_file_name(self, cdate):
        return self.pat.replace('@@', cdate)

    def _file_for_utime(self, utime):
        localtime = time.localtime(utime)
        cdate = time.strftime("%Y-%m-%d", localtime)
        return self._get_file_name(cdate)

    def log(self, lines):
        if isinstance(lines, str):
            lines = [lines]
        ctime = time.time()
        localtime = time.localtime(ctime)
        if self.bydate:
            cdate = time.strftime("%Y-%m-%d", localtime)

            cur_log_path = Path(self._get_file_name(cdate))
            if cur_log_path != self.cur_log_path:
                if self.fil:
                    self.fil.close()
                self.cur_log_path = cur_log_path
                self.fil = cur_log_path.open('a', encoding='utf8')
                if self.links:
                    logdir = self.cur_log_path.parent
                    last_file_name = None
                    utime = ctime

                    # Link 'today' and 'yesterday'
                    log_path = cur_log_path
                    for linkname in self.links:
                        # Ugly hack - going back exactly 24 hours *might* jump back two
                        # days if there is a DST change. Go back 22 hours until we get a
                        # different date.
                        while log_path == last_file_name:
                            utime -= 22*3600
                            log_path = Path(self._file_for_utime(utime))

                        last_file_name = log_path
                        if cur_log_path.exists():
                            lnk = logdir / linkname
                            lnk.unlink(missing_ok=True)

                            try:
                                lnk.symlink_to(log_path.name)
                            except OSError:
                                pass

        stime = time.strftime("%Y-%m-%d %H:%M:%S ", localtime)

        for line in lines:
            if self.stdout:
                if self.timestdout:
                    print(stime + line)
                else:
                    print(line)

        if self.fil:
            for l in lines:
                self.fil.write(stime + l + '\n')
            self.fil.flush()


def main():
    p = argparse.ArgumentParser(description='')
    p.add_argument('pipepath', type=Path, help='path to pipe')
    p.add_argument('logpath', help='log file pattern')
    p.add_argument('-l', '--links', nargs=2, help='link paths for "today" and "yesterday"')
    p.add_argument('-t', '--tee', action='store_true', help='write data to stdout in addition to log file')
    #p.add_argument(help='')
    args = p.parse_args()

    mgr = Log(args.logpath, timestdout=True, stdout=args.tee, links=args.links)

    try:
        st = os.stat(args.pipepath)
        if not stat.S_ISFIFO(st.st_mode):
            print('error: %s exists but is not pipe', file=sys.stderr)
            return
    except FileNotFoundError:
        os.mkfifo(args.pipepath)

    # Open for both reading and writing so the pipe doesn't get EOF when other processes close
    pipefd = os.open(args.pipepath, os.O_RDWR)
    dbuf = b''
    while True:
        data = os.read(pipefd, 256)
        if not data:
            break
        dbuf += data
        lines = dbuf.split(b'\n')
        dbuf = lines.pop()
        mgr.log([line.decode('utf8', 'replace') for line in lines])

if __name__ == '__main__':
    main()
