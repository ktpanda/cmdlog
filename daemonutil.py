import sys
import os
import atexit
import signal
import argparse
from typing import Optional
from pathlib import Path

options = argparse.ArgumentParser()
_got_signal = False

def readpid(pidf, daemon):
    daemon = Path(daemon).resolve()
    try:
        with pidf.open(encoding='ascii', errors='ignore') as fp:
            pid = int(fp.readline().strip())

        cmdline = Path(f'/proc/{pid}/cmdline').read_bytes().split(b'\0', 3)
        for p in cmdline[:3]:
            if Path(p).resolve() == daemon:
                return pid
    except (OSError, ValueError):
        pass
    return 0

pidfile_path = None

def exitsig(sig, frame):
    global _got_signal
    if not _got_signal:
        _got_signal = True
        raise KeyboardInterrupt

def init_options():
    options.add_argument("-p", "--pidfile", action="store",
                         dest="pidfile", metavar="FILE", type=Path,
                         help="write daemon process ID to a file")

    options.add_argument("-l", "--logfile", action="store",
                         dest="logfile", metavar="FILE", type=Path,
                         help="redirect output to log file")

    options.add_argument("-d", "--daemonize", action="store_true",
                         dest="daemon", default=False,
                         help="detach from parent")

    return options

def daemonize():
    """Makes the process into a daemon. Forks twice (to detach from
    the parent process), then calls setsid(). Closes sys.stdin,
    and redirects sys.stdout and sys.stderr to logfile, or to a dummy
    file if logfile is None.
    """

    if os.fork() != 0:
        os._exit(0)

    if os.fork() != 0:
        os._exit(0)

    os.setsid()


def init_signals():
    signal.signal(signal.SIGINT, exitsig)
    signal.signal(signal.SIGTERM, exitsig)

def init(daemon:bool=False, logfile:Optional[Path]=None, pidfile:Optional[Path]=None, close_input:bool=False):
    global pidfile_path

    null_fd = None
    log_fd = None
    pidfile_fd = None
    try:
        if logfile:
            try:
                log_fd = os.open(logfile, os.O_WRONLY|os.O_APPEND|os.O_CREAT, 0o644)
            except IOError as e:
                print(f'WARNING: could not open logfile: {e}', file=sys.stderr)

        if pidfile:
            try:
                pidfile_fd = os.open(pidfile, os.O_WRONLY|os.O_TRUNC|os.O_CREAT, 0o644)
            except IOError as e:
                print(f'WARNING: could not open pidfile: {e}', file=sys.stderr)


        if log_fd is None or close_input or daemon:
            null_fd = os.open('/dev/null', os.O_RDWR)

        if daemon:
            daemonize()

        if pidfile_fd is not None:
            os.write(pidfile_fd, f'{os.getpid()}\n'.encode('ascii'))
            pidfile_path = pidfile

        if close_input or daemon:
            os.dup2(null_fd, 0)

        stdout_fd = log_fd if log_fd is not None else null_fd
        if log_fd is not None or daemon:
            os.dup2(stdout_fd, 1)
            os.dup2(stdout_fd, 2)
    finally:
        if null_fd is not None:
            os.close(null_fd)
        if log_fd is not None:
            os.close(log_fd)
        if pidfile_fd is not None:
            os.close(pidfile_fd)

    init_signals()

    atexit.register(cleanup)

def init_from_args(args=None, close_input=True):
    if args is None:
        args = options.parse_args()
    init(daemon=args.daemon, logfile=args.logfile, pidfile=args.pidfile, close_input=close_input)

def cleanup():
    if pidfile_path:
        try:
            pidfile_path.unlink()
        except OSError:
            pass
