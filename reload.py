#!/usr/bin/python3
# PYTHON_ARGCOMPLETE_OK

import os
import argparse
import signal
import traceback
from pathlib import Path
from collections import defaultdict

import argcomplete

def read_session_file(fn, look=None):
    r = ([] if look is None else None)
    sid = None
    with open(fn, 'r', encoding='utf8', errors='replace') as f:
        for lin in f:
            lst = lin.strip().split(' ')
            if len(lst) < 2:
                if len(lst) == 1:
                    sid = lst[0]
                continue
            name = lst[0]
            if look is not None:
                if name == look:
                    return sid, lst[1:]
            else:
                r.append((name, lst[1:]))
    return sid, r

class tmuxinfo(defaultdict):
    def __missing__(self, key):
        rv = {}
        base = Path.home() / '.tmuxsave' / f'tmux-{key}'
        try:
            for session_file in base.iterdir():
                if session_file.suffix == '.session':
                    sid, pane_list = read_session_file(session_file)
                    for name, panes in pane_list:
                        rv.update(dict((int(v), (name, k)) for k, v in (x.split('=', 1) for x in panes)))
        except IOError:
            pass
        return rv

def main():
    p = argparse.ArgumentParser(description='')
    p.add_argument('-k', '--kill', action='store_true')
    p.add_argument('-a', '--all', action='store_true')
    #p.add_argument('-v', '--verbose', action='store_true', help='')
    #p.add_argument(help='')
    argcomplete.autocomplete(p, validator=lambda current_input, keyword_to_check_against: keyword_to_check_against.lower().startswith(current_input.lower()))
    #argcomplete.autocomplete(p)

    args = p.parse_args()

    reload_pids = []

    bash_path = str(Path('/bin/bash').resolve())

    for piddir in Path('/proc').iterdir():
        try:
            pid = int(piddir.name)
        except ValueError:
            continue

        try:
            exe = os.readlink(piddir / 'exe')
            if exe != bash_path:
                continue
        except IOError:
            continue

        try:
            env = [line.decode('utf8', 'replace').partition('=') for line in (piddir / 'environ').read_bytes().split(b'\0')]
            env = {k: v for k, sep, v in env if sep}
        except (IOError, ValueError):
            env = {}

        try:
            tty = os.readlink(piddir / 'fd/0')
        except IOError:
            tty = '<unknown>'

        pexe = '<unknown>'
        try:
            stat = (piddir / 'stat').read_text(encoding='utf8', errors='ignore').split()
            ppid = int(stat[3])
            pexe = os.readlink(f'/proc/{int(ppid)}/exe')

        except IOError:
            pass

        try:
            with (piddir / 'maps').open(encoding='utf8', errors='ignore') as fp:
                for line in fp:
                    if args.all:
                        need_reload = '/_cmdlog.so' in line
                    else:
                        need_reload = '/_cmdlog.so (deleted)' in line

                    if need_reload:
                        reload_pids.append((pid, tty, pexe, env))
                        break
        except IOError:
            continue

    info = tmuxinfo()

    for pid, tty, pexe, env in sorted(reload_pids):
        inst = env.get('TMUX_INSTANCE')
        pane = env.get('TMUX_PANE')
        sshdata = env.get('SSH_CONNECTION')
        tmuxdata = None
        if inst and pane:
            try:
                tmuxdata = info[inst][int(pane[1:])]
            except Exception:
                traceback.print_exc()

        print(f'{pid} ({tty}) {tmuxdata} {sshdata} {pexe}')
        if args.kill:
            os.kill(pid, signal.SIGUSR1)


if __name__ == '__main__':
    main()
