
WARNFLAGS=-Wall -Wextra -pedantic
ARCH=$(shell uname -m)

_cmdlog.so.$(ARCH): cmdlog.c
	gcc cmdlog.c -g -O2 -shared -fPIC -o _cmdlog.so.$(ARCH) -I./include $(WARNFLAGS)
