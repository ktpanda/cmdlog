
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "builtins.h"
#include "variables.h"
#include "common.h"
#include <readline/history.h>

extern char* rl_line_buffer;

static char pkt_buffer[4096];
static char prev_cmd[4096];
static int prompt_history_line = -1;

static int debug = 0;

static char cmdlog_path[256];
static char cmdlog_tty[256];

extern int bash_delete_last_history();

static int do_log(char tag, const char* command) {
    struct sockaddr_un addr;
    const char* directory;
    int size, rc = 0, sockfd;

#define tcwd the_current_working_directory

    directory = tcwd ? tcwd : get_working_directory ("cmdlog");

    size = snprintf(pkt_buffer, sizeof(pkt_buffer), "%c\001%s\001%d\001%s\001%s", tag, cmdlog_tty, getpid(), directory, command);
    if (size >= (int)sizeof(pkt_buffer)) size = sizeof(pkt_buffer) - 1;

    if (!cmdlog_path[0] || (cmdlog_path[0] == '-' && !cmdlog_path[1])) {
        printf("%c;%s;%d;%s;%s", tag, cmdlog_tty, getpid(), directory, command);
    } else {
        sockfd = socket(AF_UNIX, SOCK_DGRAM, 0);
        strncpy(addr.sun_path, cmdlog_path, sizeof(addr.sun_path));
        addr.sun_path[sizeof(addr.sun_path)-1] = 0;
        addr.sun_family = AF_UNIX;

        if (sendto(sockfd, pkt_buffer, size, 0, (const struct sockaddr*)&addr, sizeof(addr)) < 0) {
            rc = 1;
        }
        close(sockfd);
    }
    return rc;
}

static int cmdlog(WORD_LIST* list) {
    char *logcmd;
    char *text, *command = NULL;
    int rc = 0;
    HIST_ENTRY *chist, *lhist;

    if (!list || !(logcmd = list->word->word))
        goto cmderr;

    if (logcmd[0] != '-' || logcmd[1] == '\0' || logcmd[2] != '\0')
        goto cmderr;

    list = list->next;

    if (debug) {
        printf("\ncmdlog: %s\n", logcmd);
    }
    switch(logcmd[1]) {
        case 'd': debug = 1; break;
        case 'D': debug = 0; break;
        case 't':
            if (!list || !(text = list->word->word))
                goto cmderr;
            strncpy(cmdlog_tty, text, sizeof(cmdlog_tty) - 1);
            cmdlog_tty[sizeof(cmdlog_tty) - 1] = 0;
            break;
        case 's':
            if (!list || !(text = list->word->word))
                goto cmderr;
            strncpy(cmdlog_path, text, sizeof(cmdlog_path) - 1);
            cmdlog_path[sizeof(cmdlog_path) - 1] = 0;
            break;

        case 'p':
            if (prev_cmd[0]) {
                rc = do_log('-', prev_cmd);
                prev_cmd[0] = 0;
            }
            prompt_history_line = history_base + history_length - 1;
            if (debug) {
                printf("\nlhl = %d\n", prompt_history_line);
            }
            break;
        case 'c':
            if (prompt_history_line != -1) {
                int cur_history = history_base + history_length - 1;
                if (debug) {
                    printf("\nlhl = %d chl = %d\n", prompt_history_line, cur_history);
                }
                if (cur_history == prompt_history_line + 1) {
                    chist = history_get(cur_history);
                    command = chist ? chist->line : "<null>";
                    strncpy(prev_cmd, command, sizeof(prev_cmd) - 1);
                    if (debug) {
                        printf("command at %d = %s\n", cur_history, command);
                    }
                    rc = do_log('+', command);

                    lhist = history_get(prompt_history_line);
                    if (chist && lhist && !strcmp(chist->line, lhist->line)) {
                        bash_delete_last_history();
                    }
                    prompt_history_line = -1;
                }
            }
            break;
        case 'l':
            if (!list || !(command = list->word->word))
                goto cmderr;
            rc = do_log(':', command);
            break;
        default:
            goto cmderr;
    }
    /* Inline `pwd` here */

    return rc;

cmderr:
    fprintf(stderr, "usage: cmdlog -[pcl] [sockpath] [text]\n");
    return 2;
}

char * const long_doc[] = { NULL };

struct builtin _cmdlog_struct = {
    "_cmdlog",
    cmdlog,
    BUILTIN_ENABLED,
    long_doc,
    "",
    NULL
};
