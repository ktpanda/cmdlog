#
# cmdlog.setup.sh - called from .bashrc to set up command logging
#
# Copyright 2022 Katie Rust (https://ktpanda.org)
#
# Redistribution and use in source and binary forms, with or without modification, are
# permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of
# conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list
# of conditions and the following disclaimer in the documentation and/or other materials
# provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
# THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
# THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

##########################################################################################
# BEGIN COMMAND_HOOKS

# Enable other scripts to hook commands at specific points
NEWLINE='
'

_add_prompt_command() {
    cmd="${NEWLINE}$1${NEWLINE}"
    if [[ ${PROMPT_COMMAND} != *"$cmd"* ]]; then
        PROMPT_COMMAND+=$cmd
    fi
}
_add_command_hook() {
    cmd="${NEWLINE}$1${NEWLINE}"
    if [[ ${COMMAND_HOOKS} != *"$cmd"* ]]; then
        COMMAND_HOOKS+="$cmd"
    fi
}

# Set _oncmd as a DEBUG hook. Make this an alias, because we can't change the DEBUG
# hook from within the hook itself.
alias _oncmd=
trap _oncmd DEBUG

# Create a variable called COMMAND_HOOKS which behaves like PROMPT_COMMAND, but runs
# just before other commands are run. This will run from the DEBUG hook, then disable
# the hook by clearing the _oncmd alias.
declare +x COMMAND_HOOKS

_run_command_hooks() {
    eval "$COMMAND_HOOKS"
    alias _oncmd=
}

# Set up PROMPT_COMMAND to 'arm' the hook whenever an interactive prompt is
# displayed. Bash supports PROMPT_COMMAND being a string or an array, so support both
# modes.

_add_prompt_command 'alias _oncmd=_run_command_hooks'

# END COMMAND_HOOKS
##########################################################################################

# Any variables that are marked as "exported" come from the environment. Clear them.
[[ "${_cmdlog_tty@a}" == *x* ]] && _cmdlog_tty=
[[ "${_cmdlog_wasinit@a}" == *x* ]] && _cmdlog_wasinit=

declare -g +x _cmdlog_tty _cmdlog_wasinit

__nolog() { :; }
_checkreload() {
    source $_cmdlog_dir/reload/reload-$$.sh 2>/dev/null && rm -f $_cmdlog_dir/reload/reload-$$.sh
}

csetname() {
    _cmdlog_tty="`tty`[$1]"
    _cmdlog -t "$_cmdlog_tty" 2>/dev/null
}

_log_exit() { _log_command '<closed>'; }

_add_prompt_command '_checkreload; _cmdlog -p'
_add_command_hook '_checkreload; _log_current_command'

if [[ $_cmdlog_wasinit == 1 ]]; then
    return
fi

# Stubs if cmdlog can't be loaded
_start_cmdlog() { :; }
_log_current_command() { :; }
_log_command() { :; }
_cmdlog() { :; }

_cmdlog_init() {
    local logged_open
    local cmdlog_logs=$HOME/.local/logs/cmdlog
    local cmdlog_lib=$HOME/.local/lib/cmdlog


    if [ -n "$_cmdlog_tty" ]; then
        logged_open=1
    else
        _cmdlog_tty=`tty`
    fi

    enable -d _cmdlog 2>/dev/null

    if [ "`id -u`" != 0 ] && [ -z "$HISTFILE" -o "$HISTFILE" = $HOME/.bash_history ] && enable -f "$cmdlog_lib/_cmdlog.so" _cmdlog 2>/dev/null; then
        unset _cmdlog
        _cmdlog -t "!!test"
        _cmdlog -s "$cmdlog_logs/logsock"
        if ! _cmdlog -l ""; then
            echo "initializing cmdlog..."
            "$cmdlog_lib/cmdlog.py" --path "$cmdlog_logs/logsock" --dir "$cmdlog_logs" --daemon
            echo "done."
        fi
        _cmdlog -t "$_cmdlog_tty"

        _log_current_command() {
            _cmdlog -c
        }

        _log_command() {
            _cmdlog -l "$*"
        }


        export HISTCONTROL=ignorespace

    else
        _start_cmdlog() { :; }
        _log_current_command() { :; }
        _log_command() { :; }
        _cmdlog() { :; }
    fi

    [ -n "$TMUX_WINDOW" ] && csetname "$TMUX_WINDOW"

    local source="D=$DISPLAY"

    if [ -n "$SSH_CONNECTION" ]; then
        source="$source; $SSH_CONNECTION"
    fi

    if [ -z "$logged_open" ]; then
        _log_command "<opened>; $source"
        logged_open=1
    fi

    _cmdlog_wasinit=1

    trap _log_exit EXIT
}

_cmdlog_init
